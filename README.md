# Tic-Tac-Toe robot

## Overview

The goal of this project is to create a robot that plays tic-tac-toe with you on a piece of paper or a whiteboard. When starting the game, the robot draws the game grid. Then you and the robot in turn can draw X-s and O-s to the grid.

The following students work on this project:

Pair B:

* Aleksander Tamme
* Taavet Joonas Jukola

Pair A:

* Tarvi Tepandi
* Edgar Selihov

## Schedule for Pair A

Pair A is responsible for implementing the tic-tac-toe solver and controlling the robot based on video feedback.


11.11 Getting an idea on how to reliably move the marker (no actual code is written in this stage);

18.11 Code for tic-tac-toe solver complete;

26.11 Proof of concept deadline, we know where and how to use the servos to move the marker;

02.12 Robot reacts to video feedback;

03.12 Coordinating progress with pair B, making the project poster;*

07.12 Robot accurately reacts to video feedback;

09.12 Project *almost* complete, pair A and B work together, code complete;*

12.12 Project poster deadline;*

17.12 Day of live demo.*


The tasks market with an astersisk (*) are meant to be completed together by both pairs.

## Schedule for Pair B

Pair B is responsible for implementing the image processing solution for grid and X and O recognition in addition to tracking of the robot from video.

07.11 Creating a provisional weekly schedule.

14.11 Choosing which tools to use and finishing the list of components. Getting familiarized with the components.

21.11 Being able to detect the playing field, being able to differentiate between X's and O's. Making demo video for 26.11.

26.11 Being able to detect where to draw.

3.12 Coordinating progress with pair A, making the project poster.

9.12 Project *almost* complete, pair A and B work together, code complete.

12.12 Preparing for demo session, last preparations.

17.12 DAY OF DEMO SESSION

## Component list

Component name | Quantity | Needs to be provided by the course
---------------|----------|-----------------------------------
Camera |1 |yes
Raspberry pi 3 |1 |yes
Colored Marker |2 |yes
Breadboard |1 |yes
Breadboard compatible wiring |20 |yes
Continuous rotation servo |3 |yes

## Challenges and Solutions

Taavet Jukola: 

   * Challenge: Telling the robot where to write.
   
   * Solution 1: Measure the distance in pixels how much the arm moves for 1 servo rotation. Control system gets the coordinate to write in and the arm moves accordingly.
   
   * Solution 2: Color the tip of the pen and register its position with the camera. Move the arm until the pen is in the desired position. Requires additional resources for processing but is more accurate.

Aleksander Tamme:

   * Challenge: Making X/O detection more precise.
   
   * Solution 1: Writing the X's and O's in clear and precise shapes, lines shouldn't be wobbly.
   
   * Solution 2: Using different colours for X's and O's for easier filtering. (probably infeasible)

Tarvi Tepandi:

   * Challenge 1: Making the Tic-Tac-Toe solver code.
   
   * Soultion: Make it really simple, you do not need a robot that wins all the time (though it's still pretty good). It just needs to work. 
   
   * Challenge 2: Alongside Edgar, figure out the way to move the marker.
   
   * Solution: Make a system, that moves the marker on 3 different axis. Essentially this would be relatively simple to control, if we connect the 3 axes to servos and them to an arduino or a raspberry pi, straight lines would be simple to draw. 
   
   * Note concerning the solution: there was talk about using the gopigo for moving the marker though we decided it was a terrible idea and we hate the gopigo.
   
   * Challenge 3: Finding the time to actually work on the project.
   
   * Soultion: We have to get most of the materials ourselves because we can't borrow the robotics lab's materials midweek. It leads to some more challenges, but we will overcome.
   
   * Note concering the project itself: is it just me or is this course made too bothersome (sometimes difficult, sometimes too time-consuming but always bothersome) on purpose?

Edgar Selihov:
   
   * Challenge 1: Depict how Tic-Tac-Toe Robot will look

   * Solution: Take 2 wooden blocks, make rails on them. Make main wooden block move along y-axis, second along x-axis.

   * Challenge 2: Find powerful enough servomotors to make the construction move. 

   * Solution: As parallax servos are not powerful enough, they are needed to be replaced with more potent servos (6V-12V if I am not mistaken??)

   * Challenge 3: Give commands from Pi to Arduino to move servos.

   * Solution: In progress.. I guess =)
