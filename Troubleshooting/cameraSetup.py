import cv2
import numpy as np

cap = cv2.VideoCapture(0)

blobparams = cv2.SimpleBlobDetector_Params()
blobparams.minArea = 700
blobparams.maxArea = 1000000
blobparams.filterByColor = True # v2rvi j2rgi vaadata
blobparams.blobColor = 255  # 255 on valge v2rv
blobparams.filterByCircularity = False
blobparams.filterByConvexity = False
detector = cv2.SimpleBlobDetector_create(blobparams)

while True:
    ret, frame = cap.read()
    
    
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    
    
    r = [200, len(frame)-80, 250, len(frame[0])-170]
    # muudame akna suurust
    frame=frame[r[0]:r[1], r[2]:r[3]]
    
    # colour detection limits
    lB = 125
    lG = 125
    lR = 125
    hB = 255
    hG = 255
    hR = 255
    lowerLimits = np.array([lB, lG, lR])
    upperLimits = np.array([hB, hG, hR])

    # Our operations on the frame come here
    thresholded = cv2.inRange(frame, lowerLimits, upperLimits)

    # ringide leidmine 'processed' pildilt
    keypoints = detector.detect(thresholded) #saab leida ainult musti plekke
    #m2rgmine ringid originaalil
    frame = cv2.drawKeypoints(frame, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    # kirjutame midagi juurde
    for i in keypoints:
        koordinaat = (int(i.pt[0]), int(i.pt[1]))
        cv2.putText(frame, str(koordinaat), koordinaat, cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)

    
    cv2.imshow('Original', frame)
    #cv2.imshow('Orig', thresholded)
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
print('closing program')
cap.release()
cv2.destroyAllWindows()