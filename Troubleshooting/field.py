from gopigo import *
import time
enable_servo()
servo(90)

# motor 1 speed 255
# motor 2 speed 240
def motor_one(direction, len, pen):
    servo(pen)
    motor1(direction, 200)
    time.sleep(len)
    stop()
    
def motor_two(direction, len, pen):
    servo(pen)
    motor2(direction, 150)
    time.sleep(len)
    stop()

def draw_field(length = 18, width = 7.5, puhver = 11): # distances as seconds
    # top right is home
    
    # puhver
    motor_one(1, puhver, 90)
    
    motor_one(1, length, 90)
    for i in range(3):
        motor_two(1, width, 0)
    motor_one(0, length, 90)
    motor_two(0, width, 90)
    for i in range(3):
        motor_one(1, length, 0)
    motor_two(1, width, 90)
    motor_one(0, length, 90)
    for i in range(3):
        motor_two(0, width, 0)
    motor_one(1, length, 90)
    motor_two(1, width, 90)
    for i in range(3):
        motor_one(0, (length-0.7), 0)
    motor_two(0, (width-0.7), 90)
    
    # puhver
    motor_one(0, (puhver-2.3), 90) # 3 is correcture
    
    
draw_field()