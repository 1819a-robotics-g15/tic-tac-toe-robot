import numpy as np
import cv2
from time import time as t # as t, et ei tekiks konflikti mooduli ja k2su vahel

#algv22rtus aegadele, l6pp peab olema >0 et ei tekiks 0-ga jagamist
time_lopp = 1
time_algus = 0

# igale trackbarile vaikeväärtus
lB_bar = 125
lG_bar = 125
lR_bar = 125
hB_bar = 255
hG_bar = 255
hR_bar = 255

# A callback function for every trackbar
# It is triggered every time the trackbar slider is used
def updateValue1(new_value):
    # make sure to write the new value into the global variable
    global lB_bar
    lB_bar = new_value
    return
def updateValue2(new_value):
    global lG_bar
    lG_bar = new_value
    return
def updateValue3(new_value):
    global lR_bar
    lR_bar = new_value
    return
def updateValue4(new_value):
    global hB_bar
    hB_bar = new_value
    return
def updateValue5(new_value):
    global hG_bar
    hG_bar = new_value
    return
def updateValue6(new_value):
    global hR_bar
    hR_bar = new_value
    return

# open the camera
cap = cv2.VideoCapture(0)

# teeme esialgse akna kuhu paigutame trackbari
ret, frame = cap.read()
cv2.imshow('Original', frame)
# Attach a trackbar to a window named 'Original'
cv2.createTrackbar('lB', "Original", lB_bar, 255, updateValue1)
cv2.createTrackbar('lG', "Original", lG_bar, 255, updateValue2)
cv2.createTrackbar('lR', "Original", lR_bar, 255, updateValue3)
cv2.createTrackbar('hB', "Original", hB_bar, 255, updateValue4)
cv2.createTrackbar('hG', "Original", hG_bar, 255, updateValue5)
cv2.createTrackbar('hR', "Original", hR_bar, 255, updateValue6)

#ringide otsimine
#et kood n2eks k6iki suurusi, mitte ainult v2ikseid
blobparams = cv2.SimpleBlobDetector_Params()
blobparams.minArea = 100
blobparams.maxArea = 10000000
blobparams.filterByColor = True # v2rvi j2rgi vaadata
blobparams.blobColor = 255  # 255 on valge v2rv
blobparams.filterByCircularity = False # ei otsi ainult ringikujulisi
#blob detector leiab ringid mis vastavad blobparams tingimustele
blobparams.filterByConvexity = False
#filterByInertia = False
detector = cv2.SimpleBlobDetector_create(blobparams)
# tehtud on tingimused, blobe otsime tsyklis

while True:
    # palju kulub aega yhe frame kuvamiseks
    aega_kulub = time_lopp - time_algus
    # FPS arvutus
    fps = round(1 / aega_kulub, 2)
    #pildi kuvamise algus hetk
    time_algus = t()
    
    #read the image from the camera
    ret, frame = cap.read()

    # muudab v2rvid HSV systeemi
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # colour detection limits
    lB = lB_bar
    lG = lG_bar
    lR = lR_bar
    hB = hB_bar
    hG = hG_bar
    hR = hR_bar
    lowerLimits = np.array([lB, lG, lR])
    upperLimits = np.array([hB, hG, hR])

    # Our operations on the frame come here
    thresholded = cv2.inRange(frame, lowerLimits, upperLimits)

    # ringide leidmine 'processed' pildilt
    keypoints = detector.detect(thresholded) #saab leida ainult musti plekke
    #m2rgmine ringid originaalil
    frame = cv2.drawKeypoints(frame, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    # kirjutame midagi juurde
    for i in keypoints:
        koordinaat = (int(i.pt[0]), int(i.pt[1]))
        cv2.putText(frame, str(koordinaat), koordinaat, cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)

    
    cv2.imshow('Original', frame)

    
    # thresholded image
    cv2.imshow('Thresholded', thresholded)
    
    # pildi kuvamise lõpphetk
    time_lopp = t()
    
    # Quit the program when 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
print('closing program')
cap.release()
cv2.destroyAllWindows()

