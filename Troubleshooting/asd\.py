import cv2
import numpy as np


cap = cv2.VideoCapture(0)
    
    #dev = cv2.VideoCapture(1)
    #frame = cv2.imread("ruudustik2.jpg")
while True:
    ret, frame = cap.read()
    
    r = [150, len(frame)-160, 240, len(frame[0])-200]
    # muudame akna suurust
    frame=frame[r[0]:r[1], r[2]:r[3]]
    
    # muudab v2rvid HSV systeemi
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # colour detection limits
    # threshold
    lB = 10
    lG = 125
    lR = 125
    hB = 255
    hG = 255
    hR = 255
    lowerLimits = np.array([lB, lG, lR])
    upperLimits = np.array([hB, hG, hR])

    thresholded = cv2.inRange(hsv, lowerLimits, upperLimits)
    # lines white, background black
    cv2.imshow('Original', hsv)
    cv2.imshow('Orig', thresholded)
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
print('closing program')
cap.release()
cv2.destroyAllWindows()
