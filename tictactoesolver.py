#imports
import copy
from FieldScanner import detection

#functions
def getletters():
    while True:
        letter = input("Do you want to be 'X' or 'O'?")
        letter = letter.upper()
        if letter == 'X' or letter == 'O':
            break
    if letter == 'X':
        return ['X','O']
    elif letter == 'O':
        return ['O','X']

#returns True if it's the robot's turn
def whogoesfirst():
    while True:
        i = input("Do you wish to go first? (reply either 'yes' or 'no')")
        if i == 'no':
            return True
        elif i == 'yes':
            return False
        print("Something went wrong, let's try again...")

def isthereawinner(matrix,letters):
    allpossibilities = []
    #rows
    for row in matrix:
        allpossibilities += [set(row)]
    #columns
    for rowindex in range(len(matrix)):
        i = []
        for columnindex in range(len(matrix[rowindex])):
            i += [matrix[columnindex][rowindex]]
        allpossibilities += [set(i)]
    #diagonal1
    i = []
    for index in range(len(matrix)):
        i += [matrix[index][index]]
    allpossibilities += [set(i)]
    #diagonal2
    i = []
    for index in range(len(matrix)):
        i += [matrix[index][2-index]]
    allpossibilities += [set(i)]
    for i in allpossibilities:
        if i == set(letters[0]):
            return 'human'
        elif i == set(letters[1]):
            return 'robot'
    return 'draw'

def anyfreespaces(matrix):
    for row in matrix:
        for char in matrix:
            if char == ' ':
                return True
    return False

def getmatrix(lines):
    matrix = detection(lines)
    i = []
    newmatrix = []
    for char in matrix:
        if char in 'X O'.lower():
            i += [char]
        if len(i) >=3:
            newmatrix+= [i]
            i = []
    return newmatrix

def isspaceempty(matrix,pos):
    if matrix[pos[0]][pos[1]] == ' ':
        return True
    else:
        return False

def getrobotsmove(matrix,letters):
    #all possible positions with the next move for the robot
    positionsrobot = []
    for rowi in range(len(matrix)):
        for chari in range(len(matrix[rowi])):
            if matrix[rowi][chari] == ' ':
                positionsrobot += [[rowi,chari]]
    #all possible positiions with the next move for the human (for checking if the human can win with the next move)
    positionshuman = []
    for rowi in range(len(matrix)):
        for chari in range(len(matrix[rowi])):
            if matrix[rowi][chari] == ' ':
                positionshuman += [[rowi,chari]]
    #if the robot can win with the next move, do that
    for pos in positionsrobot:
        matrixcopy = copy.deepcopy(matrix)
        matrixcopy[pos[0]][pos[1]] = letters[1]
        if isthereawinner(matrixcopy,letters) == 'robot':
            return pos
    #if the human can win with the next move, block that move
    for pos in positionshuman:
        matrixcopy = copy.deepcopy(matrix)
        matrixcopy[pos[0]][pos[1]] = letters[0]
        if isthereawinner(matrixcopy,letters) == 'human':
            return pos
    corners = [[0,0],[2,2],[0,2],[2,0]]
    center = [[1,1]]
    edges = [[0,1],[2,1],[1,0],[1,2]]
    for pos in corners:
        if isspaceempty(matrix,pos):
            return pos
    for pos in corners:
        if isspaceempty(matrix,pos):
            return pos
    for pos in corners:
        if isspaceempty(matrix,pos):
            return pos

###main code initialization
##matrix = [[' ',' ',' '],[' ',' ',' '],[' ',' ',' ']]
##letters = getletters()
##isrobotsturn = whogoesfirst()
###main loop
##run = True
##while run:
##    status = isthereawinner(matrix,letters)
##    if status != 'draw':
##        print('The winner is the ' + status + '!')
##        run = False
##        break
##    
##    if anyfreespaces(matrix):
##        print("It's a draw!")
##        run = False
##        break
##    
##    if not isrobotsturn:
##        matrix = getmatrix()
##        isrobotsturn = True
##        continue
##    else:
##        pos = getrobotsmove(matrix,letters)
##        print(pos)
##        matrix[pos[0]][pos[1]] = letters[1]
##        isrobotsturn = False