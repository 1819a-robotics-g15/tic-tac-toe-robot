import time
from gopigo import *
from math import sin,cos,radians

enable_servo()

def x():
    m1 = 5
    m2 = 6
    servo(0)
    motor1(1, 200)
    time.sleep(6.5)
    stop()
    motor2(1, 150)
    time.sleep(4.5)
    stop()
    servo(90)
    motor2(1, 150)
    time.sleep(m2)
    stop()
    motor2(0, 150)
    time.sleep((m2/2)+2)
    stop()
    motor1(1, 150)
    time.sleep(m1)
    stop()
    motor1(0, 150)
    time.sleep((m1*2)-1)
    stop()
    servo(0)
    

def o():
    motor1(1, 200)
    time.sleep(5.5)
    motor2(1, 150)
    time.sleep(2)
    
    servo(90)
    t = 0
    set_right_speed(0)
    set_left_speed(0)
    forward()
    c = 70
    forward()
    while t<90:
        t+=1
        set_right_speed(abs(int((c+20)*cos(radians(t)))))
        set_left_speed((abs(int(c*sin(radians(t))))))
        time.sleep(0.1)
    right_rot()
    while t<180:
        t+=1
        set_right_speed(abs(int((c+20)*cos(radians(t)))))
        set_left_speed((abs(int(c*sin(radians(t))))))
        time.sleep(0.1)
    backward()
    while t<270:
        t+=1
        set_right_speed(abs(int((c+20)*cos(radians(t)))))
        set_left_speed((abs(int(c*sin(radians(t))))))
        time.sleep(0.1)
    left_rot()
    while t<360:
        t+=1
        set_right_speed(abs(int((c+20)*cos(radians(t)))))
        set_left_speed((abs(int(c*sin(radians(t))))))
        time.sleep(0.1)
    forward()
    while t<400:
        t+=1
        set_right_speed(abs(int((c+20)*cos(radians(t)))))
        set_left_speed((abs(int(c*sin(radians(t))))))
        time.sleep(0.1)
    servo(0)
    stop()
    