import cv2
import numpy as np

# Hough transformation info
# https://docs.opencv.org/3.4/d3/de5/tutorial_js_houghcircles.html (12.12.2018)

# in case of bad threshold
# corrects line finding
filter = 0
# narrow the sectors
# cut field lines from sector
fattener = 5
# Hough line parameter
param2_value = 9

row_number = 30

def image_processing():
    cap = cv2.VideoCapture(0)
    
    #dev = cv2.VideoCapture(1)
    #frame = cv2.imread("ruudustik2.jpg")
    ret, frame = cap.read()
    
    r = [200, len(frame)-80, 250, len(frame[0])-170]
    # muudame akna suurust
    frame=frame[r[0]:r[1], r[2]:r[3]]
    
    # muudab v2rvid HSV systeemi
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # colour detection limits
    # threshold
    lB = 28
    lG = 50
    lR = 0
    hB = 255
    hG = 255
    hR = 255
    lowerLimits = np.array([lB, lG, lR])
    upperLimits = np.array([hB, hG, hR])

    thresholded = cv2.inRange(hsv, lowerLimits, upperLimits)
    # lines white, background black
    cap.release()
    return thresholded

def line_points(thresholded = image_processing()):
    global filter
    
    # find vertical lines
    x_line = playground_lines(thresholded[row_number], filter)

    # turn matrix around
    left_edge = []
    for line in thresholded:
        left_edge += [line[row_number]]
        
    # find horizontal lines
    y_line = playground_lines(left_edge, filter)
    
    if x_line == 'empty' or y_line == 'empty':
        return 'empty'
    
    else:
        return (x_line, y_line)

def playground_lines(line, filter = 0):
    # find potential lines
    points = []
    start = None
    end = None
    for x in range(len(line)):
        if line[x] != 0:
            # find start point
            if line[x-1] == 0:
                start = x
            # find end point
            if line[x+1] == 0:
                end = x
            if start != None and end != None:
                # filter noise
                if (end - start) <= filter:
                    start = None
                    end = None
                else:
                    # add the midpoint
                    points += [int((start + end) / 2)]
                    start = None
                    end = None
    
    if len(points) == 0:
        return 'empty'
    
    # add edges
    points.insert(0, 0)
    points.append(640)
    return points

def image_splitting(lines):
    global fattener
    
    # sectors by x coordinates
    x_sectors = []
    for x_spot in range(1, len(lines[0])):
        x_sectors += [[lines[0][x_spot-1] + fattener, lines[0][x_spot] - fattener]]
    
    # sectors by y coordinates
    y_sectors = []
    for y_spot in range(1, len(lines[1])):
        y_sectors += [[lines[1][y_spot-1] + fattener, lines[1][y_spot] - fattener]]

    # generate sectors
    sectors = []
    for x_spot in x_sectors:
        for y_spot in y_sectors:
            sectors += [[y_spot, x_spot]]
    
    return sectors

def cropping(sector, frame):
    r = [len(frame)-(len(frame)-sector[0][0]), len(frame)+(sector[0][1]-len(frame)),\
         sector[1][0], len(frame[0])+(sector[1][1]-len(frame[0]))]
    frame=frame[r[0]:r[1], r[2]:r[3]]
    return frame

def circle_detection(frame):
    global param2_value
    
    circles = cv2.HoughCircles(frame,cv2.HOUGH_GRADIENT,1,80,\
                           param1=50,param2 = 9,minRadius=10,maxRadius=70)
    
    try:
        if len(circles[0]) == 1:
            return 'o'
        else:
            return 'x'
    except:
        return ' '
        
def detection(lines):
    sectors = image_splitting(lines)
    
    result = []
    
    for sector in range(len(sectors)):
        cropped_image = cropping(sectors[sector], image_processing())
        result += circle_detection(cropped_image)
    
    return result


#result = detection()

####  process for main algorithm 3x3 table
def video_scanner(lines):
    result = [[], [], []]
    i = 0
    n = 0
    for el in detection(lines):
        if i == 3:
            n += 1
            i = 0
        result[n] += [el]
        i += 1

    # turn matrix around
    result2 = [[], [], []]
    for veerg in range(len(result[0])):
        for rida in result:
            result2[veerg] += rida[veerg]
    
    return result2
    