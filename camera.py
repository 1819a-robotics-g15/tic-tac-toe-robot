import cv2
import numpy as np
from gopigo import *

def field_control(x_pos = 67, y_pos = 39): # home address
    cap = cv2.VideoCapture(0)

    blobparams = cv2.SimpleBlobDetector_Params()
    blobparams.minArea = 700
    blobparams.maxArea = 1000000
    blobparams.filterByColor = True # v2rvi j2rgi vaadata
    blobparams.blobColor = 255  # 255 on valge v2rv
    blobparams.filterByCircularity = False
    blobparams.filterByConvexity = False
    detector = cv2.SimpleBlobDetector_create(blobparams)
    
    # colour detection limits
    lB = 125
    lG = 125
    lR = 125
    hB = 255
    hG = 255
    hR = 255
    lowerLimits = np.array([lB, lG, lR])
    upperLimits = np.array([hB, hG, hR])
    
    x = 0
    y = 0
    while True:
        ret, frame = cap.read()
        
##        r = [0, len(frame)-100, 0, len(frame[0])]
##        # muudame akna suurust
##        frame=frame[r[0]:r[1], r[2]:r[3]]
        
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        
        # Our operations on the frame come here
        thresholded = cv2.inRange(frame, lowerLimits, upperLimits)

        keypoints = detector.detect(thresholded)
        
        if len(keypoints) == 0:
            stop()
        else:
            for i in keypoints:
                marker = (int(i.pt[0]), int(i.pt[1]))
                break
            
            if x != 1:
                if marker[0] in range((x_pos-5), (x_pos+5)):
                    stop()
                    x = 1
                else:
                    if marker[0] < x_pos:
                        motor1(1, 160)
                    elif marker[0] > x_pos:
                        motor1(0, 160)
                
            if y != 1:  
                if marker[1] in range((y_pos-5), (y_pos+5)):
                    stop()
                    y = 1
                else:
                    if marker[1] < y_pos:
                        motor2(1, 110)
                        
                    elif marker[1] > y_pos:
                        motor2(0, 110)
                
        if x == 1 and y == 1:
            stop()
            break
    cap.release()

def Aservo(angle):
    servo(angle)
    time.sleep(0.5)

def draw_field():
    enable_servo()
    Aservo(0)
    field_control()

    #field 
    field_control(300, 38) # was 315, 38
    Aservo(90)
    field_control(315, 392)
    Aservo(0)
    field_control(192, 276)
    Aservo(90)
    field_control(620, 276)
    Aservo(0)
    field_control(442, 392) # changed (was 457, 355)
    Aservo(90)
    field_control(457, 38)
    Aservo(0)
    field_control(620, 140) # changed (was 620, 153)
    Aservo(90)
    field_control(192, 153)
    Aservo(0)
    field_control() # back home

##servo(0)
##
##draw_field()

#motor1(0, 200)