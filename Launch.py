from camera import *
from FieldScanner import *
from tictactoesolver import *
import time
import cv2
import numpy as np
from gopigo import *
from symbol import *

enable_servo()
servo(0)
field_control()

lines = line_points()

if lines == 'empty':
    print('No field found')
    draw_field()
    lines = line_points()

print('Starting game\n')

letters = getletters()
isrobotsturn = whogoesfirst()

matrix = [[' ',' ',' '],[' ',' ',' '],[' ',' ',' ']]

field_x = (192, 315, 457)
field_y = (38, 153, 276)

run = True
while run:
    matrix = getmatrix(lines)
    print(matrix)
    status = isthereawinner(matrix,letters)
    print(status)
    if status != 'draw':
        print('The winner is the ' + status + '!')
        run = False
        break
    
    if anyfreespaces(matrix):
        print("It's a draw!")
        run = False
        break
    
    if not isrobotsturn:
        input("Press enter when you've done your move.")
        isrobotsturn = True
        continue
    else:
        pos = getrobotsmove(matrix,letters)
        ### Draw symbol
        field_control(field_x[pos[1]], field_y[pos[0]])
        if letters[1] == 'X':
            x()
            field_control()
        else:
            o()
            field_control()
        ###
        matrix[pos[0]][pos[1]] = letters[1]
        isrobotsturn = False

print('end')